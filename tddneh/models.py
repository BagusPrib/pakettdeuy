from django.db import models
from django.utils import timezone

class Stat(models.Model):
    date = models.DateField(max_length = 30, default = timezone.now)
    status = models.CharField(max_length = 300)
    
class reg(models.Model):
    name = models.CharField(max_length = 30)
    email = models.EmailField(max_length = 30)
    password = models.CharField(max_length = 30)
    def as_dict(self):
        return{
            "name": self.name,
            "email": self.email,
        }