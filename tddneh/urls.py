from django.urls import path
from .views import landpage, aboutMe, library, list, subscribe, check, submitSubscribe, subsList, getList, unsub, gLogin, login, logout

#url for app
urlpatterns = [
    path('aboutMe',aboutMe, name="aboutMe"),
    path('landpage',landpage, name="landpage"),
    path('library',library, name="library"),
    path('list/<book>', list, name="list"),
    path('subscribe', subscribe, name='subscribe'),
    path('subscribe/check', check, name="check"),
    path('subscribe/submitSubscribe', submitSubscribe, name='submitSubscribe'),
    path('subsList', subsList, name="subsList"),
    path('subsList/getList', getList, name="getList"),
    path('subsList/unsub', unsub, name="unsub"),
    path('gLogin', gLogin, name="gLogin"),
    path('login', login, name='login'),
    path('logout', logout, name='logout'),
    path('',landpage, name="landpage"),
]
