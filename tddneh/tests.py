from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth import get_user_model
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import landpage, aboutMe, library
from .models import Stat
from .forms import StatForm
import unittest
import time

# Create your tests here.

class TestTDD1(TestCase):

    def test_landpage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "landpage.html")
        
    def test_aboutMe_exist(self):
        response = Client().get('/aboutMe')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "aboutMe.html")
        
    def test_library_exist(self):
        response = Client().get('/library')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "library.html")
        
    def test_title(self):
        request = HttpRequest()
        resp = landpage(request)
        html_resp = resp.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_resp)
    
class TestModels(TestCase):
    def test_model_exist(self):
        Stat.objects.create(status="JKT 48")
        amount = Stat.objects.all().count()
        self.assertEqual(amount,1)
    
class TestForms(TestCase):
    def test_form_available(self):
        form = StatForm()
        self.assertIn("status", form.as_p())

    def test_form_empty_input(self):
        data = { 'status' : '' }
        form = StatForm(data)
        self.assertFalse(form.is_valid())
   
class TestViews(TestCase):
    def test_form_to_model(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['status'] = "ViewTest"
        landpage(request)

# class visitorTest(unittest.TestCase):
    # def setUp(self):
        # chrome_options = Options()
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('disable-gpu')
        # service_log_path = "./chromedriver.log"
        # service_args = ['--verbose']
        # self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.browser.implicitly_wait(25) 
        # super(visitorTest,self).setUp()
        
    # def tearDown(self):
        # self.browser.implicitly_wait(3)
        # self.browser.quit()
        
    # def test_create_status(self):
        # self.browser.get('http://secondofficial.herokuapp.com/')
        # time.sleep(5)
        # status_box = self.browser.find_element_by_name('status')
        # status_box.send_keys('Coba Coba')
        # status_box.submit()
        # time.sleep(5)
        # self.assertIn("Coba Coba", self.browser.page_source)
        
    # def test_header(self):
        # self.browser.get('http://secondofficial.herokuapp.com/')
        # time.sleep(5)
        # head = self.browser.find_element_by_id('name')
        # headText = head.text
        # self.assertEqual("Hello, Apa kabar?", headText)
        
    # def test_title(self):
        # self.browser.get('http://secondofficial.herokuapp.com/')
        # time.sleep(5)
        # titles = self.browser.title
        # self.assertEqual("Status.Page", titles)
        
    # def test_header_style(self):
        # self.browser.get('http://secondofficial.herokuapp.com/')
        # time.sleep(5)
        # header = self.browser.find_element_by_class_name('header')
        # image = header.value_of_css_property('background-image')
        # self.assertEqual('url("https://marvel-live.freetls.fastly.net/canvas/2018/9/cd61273ecd764c68a1744e2e683f7422?quality=95&fake=.png")', image)
        
    # def test_header_font(self):
        # self.browser.get('http://secondofficial.herokuapp.com/')
        # time.sleep(5)
        # header = self.browser.find_element_by_id('name')
        # size = header.value_of_css_property('font-size')
        # self.assertEqual('45px', size)

if __name__=='__main__':
    unittest.main(warnings='ignore')