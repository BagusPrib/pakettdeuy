from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
import google
from google.oauth2 import id_token
from google.auth.transport import requests

from .forms import StatForm, regForm
from .models import Stat, reg
import requests, webbrowser, json

response = {'author' : 'Bagus Pribadi'}

def aboutMe(request):
    return render(request, 'aboutMe.html')
    
def library(request):
    return render(request, 'library.html')
    
def landpage(request):
    stat = Stat.objects.all()
    form = StatForm(request.POST)
    if request.method == "POST":
        response['status'] = request.POST['status']
        stats = Stat(status = response['status'])
        stats.save()
        form = StatForm()
        context = {
            'form' : form,
            'stat' : stat
        }
        return render(request, 'landpage.html', context)
    else:
        form = StatForm()
        context = {
            'form' : form,
            'stat' : stat
        }
        return render(request, 'landpage.html', context)
       
def list(request, book):
    json_file = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + book)
    data = json_file.json()
    return JsonResponse(data)
    
def subscribe(request):
    form = regForm()
    context = {
        'form' : form,
    }
    return render(request, 'subscriber.html', context)
    
def check(request):
    emails = request.POST['email']
    regs = {
        'invalid': reg.objects.filter( email = emails).exists()
    }
    return JsonResponse(regs)
    
def submitSubscribe(request):
    form = regForm(request.POST)
    if form.is_valid():
        form.save()
        return JsonResponse({'kebab': "True"})
    else:
        return JsonResponse({'kebab': "False"})

def subsList(request):
    subscribers = reg.objects.all()
    return render(request, 'subsList.html')
    
def getList(request):
    subs = [obj.as_dict() for obj in reg.objects.all()]
    return JsonResponse ({"results" : subs}, content_type = 'application/json')
    
@csrf_exempt
def unsub(request) :
    if request.method == 'POST':
        email = request.POST['email']
        reg.objects.filter(email=email).delete()
        subs = [obj.as_dict() for obj in reg.objects.all()]
        return JsonResponse ({"results" : subs}, content_type = 'application/json')
        
def gLogin(request):
    return render(request, 'gLogin.html')
    
def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            id = id_token.verify_oauth2_token(token, requests.Request(),
                                                  "251329489425-576b68t5jcvjokav37g09re3586fqhl6.apps.googleusercontent.com")
            if id['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = id['sub']
            email = id['email']
            name = id['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            return JsonResponse({"status": "0", "url": reverse("library")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'gLogin.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))