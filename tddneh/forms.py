from django import forms
from .models import reg

class StatForm(forms.Form):
    attrsText = {
        'class' : 'form-control'
    }
    
    status = forms.CharField(label = "Type here...", max_length=300, widget=forms.TextInput(attrs=attrsText), required = True)
    
class regForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(regForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = ''
        self.fields['name'].widget.attrs = {'class': 'form-control', 'id': 'nameInput', 'placeholder': 'Full Name'}

        self.fields['email'].label = ''
        self.fields['email'].widget.attrs = {'class': 'form-control', 'id': 'emailInput', 'placeholder': 'Email'}
        
        self.fields['password'].label = ''
        self.fields['password'].widget = forms.PasswordInput()
        self.fields['password'].widget.attrs = {'class': 'form-control', 'id': 'passInput', 'placeholder': 'Password'}
        
    class Meta:
        model = reg
        fields = ['name', 'email', 'password']